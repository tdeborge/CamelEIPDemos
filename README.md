# FUSE-EIP

The FUSE-EIP Project will house 'simple' samples of EIP's implemented with Camel. The current patterns included are:

* Composit Message Processor
* Aggregator

The Routes all work off a same principle. The main route will pickup a file, process it and write the reuslt back to file(s)

The following modules exist:
    
## DataModelDefinition:

This module is created to separate the DataFormat Structures from the Camel Projects. This can show re-usability accross projects in order to provide an answer to Data Model Maintenance
    
## SplitJoin:

![Split Join Diagram](/splitjoin.png "The Split/Join EIP")

This module will take the input file, convert it through Bindy, split the content, transform the content in java processor, rejoin the content and write the output to JSON and XML
    
## Aggregator:

![Aggregator Diagram](/aggregator.png "The Aggregator EIP")

The Aggregator Module will read from the file, then split the message and create aggregated files (Per defined type in the file) and grouped by 10.

## EIP-Features

In order to hanle the Data Model dependency, we created a separate bundle for it. This leads to a dependency in OSGI Bundles to be deployed.
This module is generating the Features that combine the bundles ... You can use it in the following way:

* Build and install the main project
* On a successful build, you will find the features installed in your local maven repository
* On the Fuse/Karaf CLI perform the following commands to install the EIP's:
	* Add the Features URL to the system:
		* features:addurl mvn:com.rhemea.demo.eip/EIP-Features/1.0.0-SNAPSHOT/xml/features
	* Before installing features, always make sure to refresh
		* features:refreshurl
	* Check if the features contain the correct configurations:
		* features:info EIP-Aggregator
		* features:info EIP-SplitJoin
	* Install the features:
		* features:install EIP-Aggregator EIP-SplitJoin
	* Finally check if everything is running
		* osgi:list

## Used Components:

* File
* Bindy
* JAXB
* Jackson
* Aggregation Strategy
* Custom Processors