package com.rhemea.poc.aggregate.strategy;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.rhemea.demo.datamodel.aggregate.CSVDataFormat.InputFlatDataSet;

public class MyAggregationStrategy implements AggregationStrategy {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Exchange aggregate(Exchange olde, Exchange newe) {
		if(olde == null){
			ArrayList <InputFlatDataSet> al = new ArrayList();
			al.add((InputFlatDataSet) newe.getIn().getBody());
			newe.getIn().setBody(al);
			return newe;
		}
		
		((List)olde.getIn().getBody()).add((InputFlatDataSet) newe.getIn().getBody());
		return olde;
	}

}
