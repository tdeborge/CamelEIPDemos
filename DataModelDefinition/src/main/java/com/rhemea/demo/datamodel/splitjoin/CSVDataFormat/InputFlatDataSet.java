package com.rhemea.demo.datamodel.splitjoin.CSVDataFormat;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = "," , skipFirstLine = true)

public class InputFlatDataSet {
	
	@DataField(pos = 1, required = true) 
	private int	MediaType;
	
	@DataField(pos = 2, required = true) 
	private String	title;
	
	@DataField(pos = 3, required = true) 
	private	String	author;
	
	@DataField(pos = 4, required = true) 
	private String	year;

	public int getMediaType() {
		return MediaType;
	}

	public void setMediaType(int mediaType) {
		MediaType = mediaType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}
