package com.rhemea.demo.datamodel;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
