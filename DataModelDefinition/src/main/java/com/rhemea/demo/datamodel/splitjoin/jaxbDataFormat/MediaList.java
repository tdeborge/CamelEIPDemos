package com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "MediaList")
public class MediaList {

	
	String xmlFileName;
	
	
	String jsonFileName;
	
	@XmlElement (name = "BookTotal")
	int booknum = 0;
	
	@XmlElement (name = "FilmTotal")
	int filmnum = 0;
	
	@XmlElement (name = "MusicTotal")
	int musicnum = 0;

	@XmlElement (name = "Books")
	List <Book> booklist;
	
	@XmlElement ( name="Films")
	List <Film> filmlist;
	
	@XmlElement (name = "Songs")
	List <Music> musiclist;

//	public List<Book> getBooklist() {
//		return booklist;
//	}
	
	public void addBook(Book book){
		if(this.booklist == null){
			this.booklist = new ArrayList <Book> ();
		}
		this.booklist.add(book);
		booknum++;
	}
	
	public void addFilm(Film film) {
		if(this.filmlist == null){
			this.filmlist = new ArrayList <Film>();
		}
		this.filmlist.add(film);
		filmnum++;
	}
	
	public void addMusic(Music music){
		if(this.musiclist == null){
			this.musiclist = new ArrayList <Music> ();
		}
		this.musiclist.add(music);
		musicnum++;
	}

	public void setBooklist(List<Book> booklist) {
		this.booklist = booklist;
	}

//	public List<Film> getFilmlist() {
//		return filmlist;
//	}

	public void setFilmlist(List<Film> filmlist) {
		this.filmlist = filmlist;
	}

//	public List<Music> getMusiclist() {
//		return musiclist;
//	}

	public void setMusiclist(List<Music> musiclist) {
		this.musiclist = musiclist;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void setJsonFileName(String jsonFileName) {
		this.jsonFileName = jsonFileName;
	}

	@XmlElement (name = "XMLFilename" )
	public String getXmlFileName() {
		return xmlFileName;
	}
	
	@XmlElement (name = "JSONFilename")
	public String getJsonFileName() {
		return jsonFileName;
	}
}
