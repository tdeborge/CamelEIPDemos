package com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement ( name = "Film")
public class Film {
	@XmlElement ( name = "Title" )
	String	title;
	
	@XmlElement ( name = "Author" )
	private	String	author;
	
	@XmlElement ( name = "Year" )
	private String	year;

//	public String getTitle() {
//		return this.title;
//	}

	public void setTitle(String title) {
		this.title = title;
	}

//	public String getAuthor() {
//		return this.author;
//	}

	public void setAuthor(String author) {
		this.author = author;
	}

//	public String getYear() {
//		return this.year;
//	}

	public void setYear(String year) {
		this.year = year;
	}

}
