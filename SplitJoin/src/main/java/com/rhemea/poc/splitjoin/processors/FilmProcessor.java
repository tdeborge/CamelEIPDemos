package com.rhemea.poc.splitjoin.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.rhemea.demo.datamodel.splitjoin.CSVDataFormat.InputFlatDataSet;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Film;

public class FilmProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		InputFlatDataSet indata = (InputFlatDataSet) exchange.getIn().getBody();
		Film filmdata = new Film();
		
		filmdata.setAuthor(indata.getAuthor());
		filmdata.setTitle(indata.getTitle());
		filmdata.setYear(indata.getYear());
		
		exchange.getIn().setBody(filmdata);
	}

}
