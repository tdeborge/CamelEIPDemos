package com.rhemea.poc.splitjoin.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.rhemea.demo.datamodel.splitjoin.CSVDataFormat.InputFlatDataSet;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Book;

public class BookProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		//If all goes well, the Body needs to be of type InputFlatDataSet
		InputFlatDataSet indata = (InputFlatDataSet) exchange.getIn().getBody();
		Book bookdata = new Book();
		bookdata.setAuthor(indata.getAuthor());
		bookdata.setTitle(indata.getTitle());
		bookdata.setYear(indata.getYear());		
		exchange.getIn().setBody(bookdata);
	}

}
