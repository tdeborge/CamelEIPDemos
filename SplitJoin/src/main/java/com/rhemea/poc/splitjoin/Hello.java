package com.rhemea.poc.splitjoin;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
