package com.rhemea.poc.splitjoin.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.rhemea.demo.datamodel.splitjoin.CSVDataFormat.InputFlatDataSet;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Music;

public class MusicProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		InputFlatDataSet indata = (InputFlatDataSet) exchange.getIn().getBody();
		Music musicdata = new Music();
		
		musicdata.setAuthor(indata.getAuthor());
		musicdata.setTitle(indata.getTitle());
		musicdata.setYear(indata.getYear());
		
		exchange.getIn().setBody(musicdata);
	}

}
