package com.rhemea.poc.splitjoin.aggregators;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Book;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Film;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.MediaList;
import com.rhemea.demo.datamodel.splitjoin.jaxbDataFormat.Music;

public class AggregateMedia implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		Object content = newExchange.getIn().getBody();
		if(oldExchange == null){
			MediaList ml = new MediaList();		
			addMediaToList(ml, content);	
			newExchange.getIn().setBody(ml);
			ml.setJsonFileName(newExchange.getIn().getHeader("CamelFilenameOnly") + ".json");
			ml.setXmlFileName(newExchange.getIn().getHeader("CamelFilenameOnly") + ".xml");
			return newExchange;
		}
		
		MediaList oml = (MediaList) oldExchange.getIn().getBody();
		addMediaToList(oml, content);
		
		return oldExchange;
	}
	
	public void addMediaToList(MediaList ml, Object item){
		
//		System.out.println("Adding content to the MediaList");
		
		if(item instanceof Book){
			ml.addBook((Book) item);
		}
		else if(item instanceof Film){
			ml.addFilm((Film) item); 
		}
		else{
			ml.addMusic((Music) item);
		}		
	}

}
